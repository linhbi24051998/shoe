<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Brand::class, function (Faker $faker) {
    return [
        'name' => 'Adidas'.$faker->numberBetween(1,50),
        'content' => 'Adidas'.$faker->numberBetween(1,50),
    ];
});
