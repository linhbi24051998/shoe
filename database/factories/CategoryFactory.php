<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models;
use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'name' => 'Super'.$faker->numberBetween(1,50),
        'content' => 'super'.$faker->numberBetween(1,50),
    ];
});
