<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\ImagesProduct::class, function (Faker $faker) {
    return [
        'name' => 'Product'.$faker->numberBetween(1,5).'jpg',
        'product_id' => $faker->numberBetween(1,15)
    ];
});
