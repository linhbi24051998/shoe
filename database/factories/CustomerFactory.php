<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'avatar' => 'default.jpg',
        'address' =>'Ha Noi'.$faker->numberBetween(1,100),
        'phone'=>'111-222-333-44',
        'email' => $faker->unique()->safeEmail,
        'password' => '123',
        'note' => 'Khach hang moi',
        'gender' => $faker->numberBetween(0,1),
        'status'=>'1',
    ];
});
