<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'category_id' => $faker->numberBetween(1,15),
        'brand_id' => $faker->numberBetween(1,15),
        'name' => $faker->name(),
        'content' => 'Hàng chất lượng'.$faker->numberBetween(1,50),
        'status' => $faker->numberBetween(0,1),
        'number' => $faker->numberBetween(1,10),
        'price' => $faker->numberBetween(500,1000),
        'image' => 'product'.$faker->numberBetween(1,5).'.jpg'
    ];
});
