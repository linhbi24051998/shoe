<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Category::class,15)->create();
        factory(App\Models\Brand::class,15)->create();
        factory(App\Models\Customer::class,15)->create();
        factory(App\Models\Product::class,15)->create();
        factory(App\Models\ImagesProduct::class,15)->create();
        factory(App\Models\User::class,10)->create();
        factory(App\Models\Role::class,5)->create();
        factory(App\Models\RoleUser::class,3)->create();
    }
}
