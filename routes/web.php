<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(array("prefix" => "admin", 'namespace' => 'Admins', 'middleware' => ['auth', 'role:superadmin']), function () {
    Route::get('home', 'DashboardController@index')->name('admin.home');

    Route::group(array('prefix' => 'category'), function () {
        Route::get('/', 'CategoryController@index')->name('categories');
        Route::get('/{id}', 'CategoryController@show')->name('category');
        Route::post('/', 'CategoryController@store')->name('category.add');
        Route::post('/{id}', 'CategoryController@update')->name('category.update');
        Route::delete('/{id}', 'CategoryController@destroy')->name('category.delete');
    });

    Route::group(array('prefix' => 'brands'), function () {
        Route::get('/', 'BrandController@index')->name('brands.index');
        Route::get('/{id}', 'BrandController@show')->name('brands.show');
        Route::post('/', 'BrandController@store')->name('brands.store');
        Route::post('/{id}', 'BrandController@update')->name('brands.update');
        Route::delete('/{id}', 'BrandController@destroy')->name('brands.destroy');

    });

    Route::group(['prefix' => 'customers', 'middleware' => 'role:admin'], function () {
        Route::get('/', 'CustomerController@index')->name('customers.index');
        Route::get('/{id}', 'CustomerController@show')->name('customer.show');
        Route::post('/{id}', 'CustomerController@changeStatus')->name('customers.changeStatus');
        Route::delete('/{id}', 'CustomerController@destroy')->name('customer.destroy');
        Route::delete('multi-delete/{id}', 'CustomerController@multiDelete')->name('customer.destroy');
    });

    Route::group(['prefix' => 'products', 'middleware' => 'role:admin'], function () {
        Route::get('/', 'ProductController@index')->name('product.index');
        Route::get('/create', 'ProductController@create')->name('product.create');
        Route::post('/', 'ProductController@store')->name('product.store');
        Route::get('/edit/{id}', 'ProductController@edit')->name('product.edit');
        Route::post('/{id}', 'ProductController@update')->name('product.update');
        Route::delete('/{id}', 'ProductController@destroy')->name('product.delete');
        Route::delete('/edit/image_detail/{id}', 'ImagesProductController@destroy')->name('image.delete');
        Route::delete('/multi-delete/{id}', 'ProductController@multiDelete');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@index')->name('users.index');
        Route::get('/create', 'UserController@create')->name('users.create');
        Route::post('/', 'UserController@store')->name('users.store');
        Route::get('/{id}', 'UserController@edit')->name('users.edit');
        Route::post('/{id}', 'UserController@update')->name('users.update');
        Route::delete('/{id}', 'UserController@destroy')->name('users.delete');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('page-error', function () {
    return view('admin.error');
})->name('page-error');
