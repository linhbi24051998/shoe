<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = [
        'name',
        'address',
        'avatar',
        'phone',
        'email',
        'password',
        'note',
        'status'
    ];

    public function getAll()
    {
        return $this->select('id', 'name', 'address', 'avatar', 'phone', 'email', 'note', 'status')->latest('id')->paginate(5);
    }

    public function updateStatus($id)
    {
        $customer = Customer::findOrFail($id);
        if ($customer->status == 1) {
            $customer->update(['status' => 0]);
        } else {
            $customer->update(['status' => 1]);
        }
        return $customer;
    }
}
