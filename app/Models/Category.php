<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'content',
    ];

    public function search($name)
    {
        return $this->where('name', 'LIKE', '%' . $name . '%')->latest('id')->paginate(6);
    }

    public function product()
    {
        return $this->hasMany(Product::class);

    }

}
