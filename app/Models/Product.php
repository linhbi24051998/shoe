<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'id',
        'category_id',
        'brand_id',
        'name',
        'content',
        'image',
        'status',
        'number',
        'price',
    ];

    public function search($data)
    {
        $name = $data['name'] ?? null;
        $categoryName = $data['category'] ?? null;
        $brandName = $data['brand'] ?? null;
        return $this->where('name', 'LIKE', '%' . $name . '%')
            ->when($categoryName, function ($query) use ($categoryName) {
                $query->whereHas('category', function ($q) use ($categoryName) {
                    $q->where('name', $categoryName);
                });
            })
            ->when($brandName, function ($query) use ($brandName) {
                $query->whereHas('brand', function ($q) use ($brandName) {
                    $q->where('name', $brandName);
                });
            })
            ->latest()->paginate(10);
    }

    public function category()
    {
        return $this->belongsto(Category::class);
    }

    public function brand()
    {
        return $this->belongsto(Brand::class);
    }

    public function images()
    {
        return $this->hasMany(ImagesProduct::class);
    }

    public function getAll()
    {
        return $this->latest('id')->paginate(8);
    }

}
