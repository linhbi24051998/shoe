<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImagesProduct extends Model
{
    protected $table = 'images_products';
    protected $fillable = [
        'product_id',
        'name'
    ];

    public function product()
    {
        return $this->belongsto(Product::class);
    }
}
