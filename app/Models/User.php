<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_users', 'user_id', 'role_id')->withPivot('user_id', 'role_id');
    }

    public function search($data)
    {
        $name = $data['name'] ?? null;
        $roleName = $data['role'] ?? null;
        return $this->where('name', 'LIKE', '%' . $name . '%')
            ->when($roleName, function ($query) use ($roleName) {
                $query->whereHas('roles', function ($q) use ($roleName) {
                    $q->where('name', $roleName);
                });
            })
            ->latest('id')->paginate(7);
    }

    public function getAll()
    {
        return $this->latest('id')->paginate(8);
    }

    public function hasRole($role)
    {
        return $this->roles()->where('name', $role)->first() !== null ;
    }

    public function hasAnyRole($roles)
    {
        return $this->roles()->whereIn('name', $roles)->first() !== null ;
    }

    public function authorizeRole($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles);
        }
        return $this->hasRole($roles);
    }
}
