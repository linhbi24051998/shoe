<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = [
        'name',
        'content',
    ];

    public function search($name)
    {
        return $this->where('name', 'LIKE', '%' . $name . '%')
            ->latest('id')->paginate(6);
    }

    public function product()
    {
        return $this->hasMany(Product::class);

    }
}
