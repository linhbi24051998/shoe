<?php

namespace App\Traits;

trait ImageTrait
{
    function uploadImage($file, $path)
    {
        $imgName = $file->getClientOriginalName();
        $imgName = time() . '_' . $imgName;
        $file->move($path, $imgName);
        return $imgName;
    }

    function deleteImage($image, $path)
    {
        $image_path = $path . $image;
        if (file_exists($image_path) && $image != 'default.jpg') {
            unlink($image_path);
        }
    }

    function updateMultiImage($images, $path)
    {
        foreach ($images as $image) {
            $this->uploadImage($image, $path);
        }
    }

    function deleteMultiImage($images, $path)
    {
        foreach ($images as $image) {
            $this->deleteImage($image, $path);
        }
    }
}
