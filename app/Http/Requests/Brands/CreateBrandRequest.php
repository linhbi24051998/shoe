<?php

namespace App\Http\Requests\Brands;

use App\Rules\ShortStringLength;
use Illuminate\Foundation\Http\FormRequest;

class CreateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100', 'unique:brands,name', new ShortStringLength()],
            'content' => ['required', new ShortStringLength()]
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '*Tên không được để trống',
            'name.max' => '* Tên phải nhỏ hơn 100',
            'name.unique' => '*Tên đã có! Hãy thử tên khác*',
            'content.required' => '*Nội dung không được để trống',
        ];
    }
}
