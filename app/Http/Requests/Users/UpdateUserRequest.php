<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'name' => 'required',
            'avatar' => 'mimes:jpeg,png,jpg',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'email không được để trống',
            'name.required' => 'tên không được bỏ trống',
            'avatar.mines' => 'avatar phải là ảnh'
        ];
    }
}
