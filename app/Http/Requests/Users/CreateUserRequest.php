<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users',
            'name' => 'required',
            'avatar' => 'required|mimes:jpeg,png,jpg',
            'role' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'email không được để trống',
            'email.unique' => 'email đã tồn tại',
            'name.required' => 'tên không được để trống',
            'avatar.required' => 'avatar không được bỏ trống',
            'avatar.mimes' => 'avatar phải là ảnh',
            'role.required' => 'phải chọn quyền'
        ];
    }
}
