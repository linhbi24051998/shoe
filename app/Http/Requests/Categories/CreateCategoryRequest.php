<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:100|unique:categories,name,' . $this->category,
            'content' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '*Tên không được để trống',
            'name.max' => '* Tên phải nhỏ hơn 100',
            'name.min' => '* Tên phải lớn hơn 5',
            'name.unique' => '*Tên đã có! Hãy thử tên khác*',
            'content.required' => '*Nội dung không được để trống',
            'content.min' => '*Nội dung phải lớn hơn 5',
        ];
    }
}
