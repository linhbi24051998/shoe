<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:500',
            'content' => 'required|min:5',
            'price' => 'required|numeric',
            'number' => 'required',
            'image' => 'mimes:jpeg,png,jpg',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '*Tên không được để trống',
            'name.max' => '* Tên phải nhỏ hơn 500',
            'name.min' => '* Tên phải lớn hơn 5',
            'content.required' => '*Tên không được để trống',
            'content.min' => '* Tên phải lớn hơn 5',
            'price.required' => '* Không được để trống',
            'price.numeric' => '* Phải là số',
            'number.required' => '* Số lượng không được để trống',
            'image.mines' => '* Phải là ảnh',
        ];
    }
}

