<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = Auth::user();
        if ($user->authorizeRole($role)) {
            return $next($request);
        } else {
            return redirect()->route('page-error')->with('message', 'Không được phép truy cập');
        }

    }
}
