<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Brands\CreateBrandRequest;
use App\Http\Requests\Brands\UpdateBrandRequest;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * @var Brand
     */
    protected $brand;

    public function __construct()
    {
        $this->brand = new Brand;
    }

    public function index(Request $request)
    {
        $brands = $this->brand->search($request->name);
        return view('admin.brands.index', compact('brands'));
    }

    /**
     * @param CreateBrandRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateBrandRequest $request)
    {
        $requestData = $request->all();
        $data = $this->brand->create($requestData);
        return response()->json([
            'data' => $data,
            'status' => 200,
            'message' => 'Create successful',
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = $this->brand->findOrFail($id);
        return response()->json([
            'message' => 'Get successful',
            'status' => 200,
            'data' => $data
        ]);
    }

    /**
     * @param UpdateBrandRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBrandRequest $request, $id)
    {
        $requestData = $request->all();
        $data = $this->brand->findOrFail($id);
        $data->update($requestData);
        return response()->json([
            'data' => $data,
            'status' => 200,
            'message' => 'Update successful',
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Brand::destroy($id);
    }
}
