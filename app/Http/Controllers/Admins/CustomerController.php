<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Traits\ImageTrait;

class CustomerController extends Controller
{
    use ImageTrait;
    public $path = 'admin/upload/customers/';
    protected $customer;

    public function __construct()
    {
        $this->customer = new Customer;
    }

    public function index()
    {
        $customers = $this->customer->getAll();
        return view('admin.customers.index', compact('customers'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = $this->customer->findOrFail($id);
        return response()->json([
            'message' => 'get successful',
            'data' => $data
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus($id)
    {
        $customer = $this->customer->updateStatus($id);
        return response()->json([
            'message' => 'change status successful',
            'data' => $customer
        ]);
    }

    public function destroy($id)
    {
        $customer = $this->customer->findOrFail($id);
        $this->deleteImage($customer->avatar, $this->path);
        $customer->delete();
    }

    public function multiDelete($id)
    {
        $ids = explode(",", $id);
        $imagesCustomer = $this->customer->find($ids)->pluck('avatar');
        $this->deleteMultiImage($imagesCustomer, $this->path);
        Customer::destroy($ids);
    }
}

