<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\CreatCategoryRequest;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category;

    public function __construct()
    {
        $this->category = new Category;
    }

    public function index(Request $request)
    {
        $categories = $this->category->search($request->name);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * @param CreateCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateCategoryRequest $request)
    {
        $requestData = $request->all();
        $data = $this->category->create($requestData);
        return response()->json([
            'status' => 200,
            'message' => 'Create successful',
            'data' => $data
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = $this->category->findOrFail($id);
        return response()->json([
            'status' => 200,
            'message' => 'get successful',
            'data' => $data
        ]);
    }

    /**
     * @param UpdateCategoryRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCategoryRequest $request, $id)
    {

        $requestData = $request->all();
        $data = $this->category->findOrFail($id);
        $data->update($requestData);
        return response()->json([
            'status' => 200,
            'message' => 'update successful',
            'data' => $data
        ]);
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $this->category->destroy($id);
    }
}
