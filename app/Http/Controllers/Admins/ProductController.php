<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\CreateProductRequest;
use App\Http\Requests\Products\UpdateProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ImagesProduct;
use App\Models\Product;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ImageTrait;
    protected $product;
    public $path = 'admin/upload/products/';

    public function __construct()
    {
        $this->product = new Product;
    }

    public function index(Request $request)
    {
        $categories = Category::all();
        $brands = Brand::all();
        $dataProduct = $request->only(['name', 'category', 'brand']);
        $products = $this->product->search($dataProduct);
        return view('admin.products.index', compact('products', 'categories', 'brands'));
    }

    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('admin.products.create', compact('categories', 'brands'));
    }

    public function store(CreateProductRequest $request)
    {
        $dataRequest = $request->all();
        $imageName = $this->uploadImage($request->file('image'), $this->path);
        $dataRequest['image'] = $imageName;
        $product = $this->product->create($dataRequest);
        $product_id = $product->id;

        if ($request->hasFile('uploadFile')) {
            foreach ($request->file('uploadFile') as $file) {
                $images_detail = new ImagesProduct();
                if (isset($file)) {
                    $pathImage = 'admin/upload/products/images_detail';
                    $images_detail->name = $this->uploadImage($file, $pathImage);
                    $images_detail->product_id = $product_id;
                    $images_detail->save();
                }
            }
        }
        return redirect()->route('product.index')->with('message');
    }

    public function edit($id)
    {
        $categories = Category::all();
        $brands = Brand::all();
        $product = $this->product::findOrFail($id);
        $images = ImagesProduct::all()->where('product_id', $id);
        return view('admin.products.edit', compact('product', 'categories', 'brands', 'images'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $product = $this->product::findOrFail($id);
        $dataProduct = $request->all();
        if ($request->hasFile('image')) {
            $this->deleteImage($product->image, $this->path);
            $imgName = $this->uploadImage($request->file('image'), $this->path);
            $dataProduct['image'] = $imgName;
        }
        if ($request->hasFile('uploadFile')) {
            foreach ($request->file('uploadFile') as $file) {
                $images_detail = new ImagesProduct();
                if (isset($file)) {
                    $path = 'admin/upload/products/images_detail';
                    $images_detail->name = $this->uploadImage($file, $path);
                    $images_detail->product_id = $id;
                    $images_detail->save();
                }
            }
        }
        $product->update($dataProduct);
        return redirect()->route('product.index')->with('message');
    }

    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $this->deleteImage($product->image, $this->path);
        $product->delete();
    }

    public function multiDelete($id)
    {
        $ids = explode(",", $id);
        $imagesProduct = $this->product->find($ids)->pluck('image');
        $this->deleteMultiImage($imagesProduct, $this->path);
        $this->product->destroy($ids);
    }

}

