<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\ImagesProduct;
use App\Traits\ImageTrait;

class ImagesProductController extends Controller
{
    use ImageTrait;
    public $path = 'admins/products/images_detail/';

    public function destroy($id)
    {
        $imageProduct = ImagesProduct::findOrFail($id);
        $this->deleteImage($imageProduct->name, $this->path);
        $imageProduct->delete();
    }
}
