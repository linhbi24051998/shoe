<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;

class DashboardController extends Controller
{
    public function index()
    {
        $totalBrand = Brand::count();
        $totalCategory = Category::count();
        $totalProduct = Product::count();
        $products = Product::where('status', 1)->paginate(4);
        $customers = Customer::where('status', 1)->paginate(4);
        return view('admin.home', compact('products', 'customers', 'totalBrand', 'totalCategory', 'totalProduct'));
    }
}
