<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ImageTrait;
    protected $users;
    protected $path = 'admin/upload/users/';

    public function __construct()
    {
        $this->user = new User;
    }

    public function index(Request $request)
    {
        $roles = Role::all();
        $dataUser = $request->only(['name', 'role']);
        $users = $this->user->search($dataUser);
        return view('admin.users.index', compact('users', 'roles'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create', compact('roles'));
    }

    public function store(CreateUserRequest $request)
    {
        $dataRequest = $request->all();
        $avatar = $this->uploadImage($request->file('avatar'), $this->path);
        $dataRequest['avatar'] = $avatar;
        $user = $this->user->create($dataRequest);
        $user->roles()->attach($dataRequest['role']);
        return redirect()->route('users.index')->with('message');
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user = $this->user::findOrFail($id);
        $listRoles = $user->roles()->pluck('role_id')->toArray();
        return view('admin.users.edit', compact('user', 'roles', 'listRoles'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->user->findOrFail($id);
        $dataRequest = $request->all();
        if ($request->hasFile('avatar')) {
            $this->deleteImage($user->avatar, $this->path);
            $avatar = $this->uploadImage($request->file('avatar'), $this->path);
            $dataRequest['avatar'] = $avatar;
        }
        $user->update($dataRequest);
        $user->roles()->sync($dataRequest['role']);
        return redirect()->route('users.index')->with('message');
    }

    public function destroy($id)
    {
        $user = $this->user->findOrFail($id);
        $this->deleteImage($user->avatar, $this->path);
        $user->delete();
    }
}
