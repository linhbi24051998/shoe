$('#new-category').click(function (event) {
    event.preventDefault();
    let dataResource = $('#new-category-form').serialize();
    let urlResource = '/admin/category';
    callAjax(urlResource, dataResource,'post')
        .done(response => {
            alertSucess(response.message);
            let categoryRow =  fillCategoryToRowTable(response.data);
            $("#listCategories").prepend(categoryRow);
            $('.name_add').val("");
            $('.content_add').val("");
            $('.notice_category_add').hide();
        })
        .fail(error => {
            $('.notice_category_add').show();
            let errors = error.responseJSON.errors;
            showError('.notice_category_add',errors)
        })
});

let categoryId = 0;
$(document).on('click','.editCategory', function () {
    categoryId = $(this).attr('editId');
    let dataCategory = null;
    let url = '/admin/category/' + categoryId;
    callAjax(url, dataCategory, 'get')
        .done(response => {
            $('.name_edit').val(response.data.name);
            $('.content_edit').val(response.data.content);
            $('.notice_edit').hide();
        })
        .fail(error => {
            $('.notice_edit').show();
            let errors = error.responseJSON.errors;
            let str = '.notice_add';
            showError(str,errors);
        })
});

$('#update_category').click(function (event){
    event.preventDefault();
    let dataUpdate = $('#edit-category-form').serialize();
    let urlResource = '/admin/category/'+ categoryId;

    callAjax(urlResource, dataUpdate,'post')
        .done(response => {
            $('.edit_category').modal('hide');
            let categoryRow = fillCategoryToRowTable(response.data)
            $(".editCategory[editId="+categoryId+"]").parents('tr').replaceWith(categoryRow);
            alertSucess(response.message);
            $('.notice_category_edit').hide();
        })
        .fail(error => {
            $('.notice_category_edit').show();
            let errors = error.responseJSON.errors;
            showError('.notice_category_edit',errors);
        })
});

$(document).on('click', '.deleteCategory', function(){
    categoryId = $(this).data('id');
    let urlDelete =  '/admin/category/' + categoryId;
    destroyByAjax(urlDelete)
        .then(data =>{
            alertSucess(data.message);
            $(this).parents('tr').remove();
        })
        .catch(data => {
            alertError(data.message);
        })
});
