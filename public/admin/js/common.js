$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function callAjax(url, data = "", type = '') {
    return $.ajax({
        url: url,
        type: type,
        data: data,
    });
}

function alertError(message) {
    Swal.fire({
        icon: 'error',
        title: 'Oops...:',
        text: message,
    })
}

function alertSucess(message) {
    Swal.fire({
        icon: 'success',
        title: 'OK!',
        text: message,
    })
}

function destroyByAjax(url) {
    return new Promise(function (resolve, reject) {
        Swal.fire({
            title: 'Xác nhận xóa?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa ',
            cancelButtonText: 'Hủy bỏ'
        }).then((result) => {
            if (result.value) {
                callAjax(url, null, 'delete')
                    .done(data => {
                        resolve(data);
                    })
                    .fail(data => {
                        reject(data);
                    });
            }
        });
    })
}

function fillCategoryToRowTable(data) {
    return `<tr class="even pointer">
                <td class=" ">${data.name}</td>
                <td class=" ">${data.content}</td>
                <td class=" ">
                    <a href="#" class="btn btn-info btn-xs editCategory" data-toggle="modal" data-target=".edit_category" editId="${data.id}" editName="${data.name}"><i class="fa fa-pencil"></i> Sửa </a>
                    <a href="#" class="btn btn-danger btn-xs deleteCategory" editId="${data.id}" ><i class="fa fa-trash-o"></i> Xóa </a>
                </td>
            </tr>`;
}

function showError(str, errors) {
    (errors.content) ? $(str).html(errors.content) : "";
    (errors.name) ? $(str).html(errors.name) : "";
}

function fillBrandToRowTable(data) {
    return `<tr class="even pointer">
                <td class=" ">${data.name}</td>
                <td class=" ">${data.content}</td>
                <td class=" ">
                    <a href="#" class="btn btn-info btn-xs editBrand" data-toggle="modal" data-target=".edit_brand" editbraid="${data.id}" editName="${data.name}" ><i class="fa fa-pencil"></i> Sửa </a>
                    <a href="#" class="btn btn-danger btn-xs deleteBrand" editbraid="${data.id}" ><i class="fa fa-trash-o"></i> Xóa </a>
                </td>
            </tr>`;
}

function fillCustomerButton(data) {
    return `<button class=" btn btn-primary block" id="sts_${data.id}" data-id="${data.id}">${data.status == 1 ? 'Chặn' : 'Cho phép'}</button>`
}

function fillCustomerStatus(data) {
    return `<td class="a-right a-right " class="change_status_${data.id}">
              ${data.status == 1 ? 'cho phép' : 'chặn'}
            </td>`;
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.image-show').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(".image-input").change(function () {
    readURL(this);
});


$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
