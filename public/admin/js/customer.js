$(document).on('click', '#check-all', function(){
    $(".check_one").prop("checked",$(this).prop("checked"));
});
let id_customer = 0;
$(document).on('click', '.block', function(){
    id_customer = $(this).data('id');
    let url = "/admin/customers/" + id_customer;
    callAjax(url, null, 'post')
        .done(response =>{
            let btn = fillCustomerButton(response.data);
            let status = fillCustomerStatus(response.data);
            $(".change_status[replace="+id_customer+"]").html(status);
            $("#sts_"+id_customer).replaceWith(btn);
        })
});
$('.delete_customer').click(function () {
    id_customer = $(this).data('id');
    let urlDelete = '/admin/customers/' + id_customer;
    destroyByAjax(urlDelete)
        .then(data => {
            alertSucess(data.message);
            $(this).parents('tr').remove();
        })
        .catch(data => {
            alertError(data.message);
        })
});
$('#delete_all').click(function () {
    let ids = [];
    $('.check_one:checked').each(function () {
        ids.push($(this).attr('data-id'));
    });
    let urlDelete = '/admin/customers/multi-delete/' + ids;
    destroyByAjax(urlDelete)
        .then(data => {
            alertSucess(data.message);
            $.each(ids, function (key, value) {
                $("#delete_" + value).remove();
            })
        })
        .catch(data => {
            alertError(data.message);
        })
});
