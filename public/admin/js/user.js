$('.delete_user').click(function () {
    let user_id = $(this).attr('editUser');
    console.log(user_id);
    let urlDelete = '/admin/users/' + user_id;
    destroyByAjax(urlDelete)
        .then(data => {
            alertSucess('Xóa thành công');
            $(this).parents('tr').remove();
        })
        .catch(error => {
            alertError(error.errors);
        })
});
