let product_id = 0;
$(document).on('click','.view_product', function () {
    product_id = $(this).data('id');
    let data = null;
    let url = '/admin/products/' + product_id;
    callAjax(url, data, 'get')
        .done(response => {
            console.log(response.data);
            console.log(response.images);
            $('.x_panel').html(viewProduct(response.data, response.images));

        })
});

$(document).on('click', '#check_multiple', function(){
    $(".check_1").prop("checked",$(this).prop("checked"));
});
let product_ids = [];
$(".delete_multiple").click(function () {
    $('.check_1:checked').each(function () {
        product_ids.push($(this).attr('data-id'));
    });
    console.log(product_ids);
    let urlDelete = '/admin/products/multi-delete/' + product_ids;
    destroyByAjax(urlDelete)
        .then(data => {
            alertSucess(data.message);
            $.each(product_ids, function (key, value) {
                $("#check_delete_" + value).remove();
            })
        })
        .catch(data => {
            alertError(data.message);
        })
});

$('.delete_product').click(function () {
    let product_id =  $(this).attr('data-id');
    let urlDelete = '/admin/products/' + product_id;
    destroyByAjax(urlDelete)
        .then(data => {
            alertSucess('Xóa thành công');
            $(this).parents('tr').remove();
        })
        .catch(data => {
            alertError(data.message);
        })
});

$(document).on('mouseover', '.image_detail', function(){
    let id = $(this).data('id');
    $('.change_btn_'+id).show();
});

$(document).on('mouseout', '.image_detail', function(){
    $('.change_btn').hide();
});

$(document).on('click', '.change_btn', function () {
    let id_image = $(this).attr('id_image');
    let url = '/admin/products/edit/image_detail/'+ id_image;
    destroyByAjax(url)
        .then(data => {
            alertSucess('Xóa thành công');
            $(this).parents('src').remove();
        })
        .catch(data => {
            alertError(data.message);
        })
});

