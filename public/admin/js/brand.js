
$('#new-brand').click(function (event) {
    $('.notice_brand').hide();
    event.preventDefault();
    let dataResource = $('#new-brand-form').serialize();
    let urlResource = '/admin/brands/';
    callAjax(urlResource, dataResource,'post')
        .done(response => {
            let brandRow =  fillBrandToRowTable(response.data);
            $("#listBrands").prepend(brandRow);

            $('.notice_add').hide();
            $('.name_add').val("");
            $('.content_add').val("");
            alertSucess(response.message);
        })
        .fail(error => {
            $('.notice_add').show();
            let errors = error.responseJSON.errors;
            let str = '.notice_add';
           showError(str,errors);
        })

});
let brandId = 0;
$(document).on('click','.editBrand', function () {
    brandId = $(this).attr('editbraId');
    let data = null;
    let url = '/admin/brands/' + brandId;
    callAjax(url, data, 'get')
        .done(response => {
            $('.name').val(response.data.name);
            $('.content').val(response.data.content);
        })
});

$('#update_brand').click(function (event){
    event.preventDefault();
    let dataUpdate = $('#edit-brand-form').serialize();
    // console.log(dataUpdate);
    let urlResource = '/admin/brands/'+ brandId;
    callAjax(urlResource, dataUpdate,'post')
        .done(response => {
            $('.edit_brand').modal('hide');
            let brandRow = fillBrandToRowTable(response.data);
            $(".editBrand[editbraId="+brandId+"]").parents('tr').replaceWith(brandRow);
            alertSucess(response.message);
            $('.notice_edit').hide();
            $('.name').val("");
            $('.content').val("");
        })
        .fail(error => {
            $('.notice_edit').show();
            let errors = error.responseJSON.errors;
            showError('.notice_edit',errors);
        })
});

$(document).on('click', '.deleteBrand', function(){
    brandId = $(this).attr('editbraId');
    let urlDelete =  '/admin/brands/' + brandId;
    destroyByAjax(urlDelete)
        .then(data =>{
            alertSucess(data.message);
            $(this).parents('tr').remove();
        })
        .catch(data => {
            alertError(data.message);
        })
});
