<style>
    .notice_category_add{
        color: red;
    }
</style>
<div class="modal fade add_category" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Thêm mới danh mục</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="new-category-form"
                      enctype="multipart/form-data">
                    <div class="modal-body text-dark ">
                        @csrf
                        <div class="form-group">
                            <label for="">Tên hiện thị</label>
                            <input type="text" class="form-control name_add" name="name"
                                   required>
                            <label for="">Nội dung</label>
                            <input type="text" class="form-control content_add" name="content"
                                   required>
                        </div>
                        <div class="notice_category_add"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" id="new-category"><i class="fa fa-plus">
                                Thêm mới
                            </i>
                        </button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Trở về
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
