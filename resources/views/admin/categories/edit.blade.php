<style>
    .notice_category_edit{
        color: red;
    }
</style>
<div class="modal fade edit_category" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Sửa danh mục</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="edit-category-form"
                      enctype="multipart/form-data">
                    <div class="modal-body text-dark ">
                        @csrf
                        <div class="form-group">
                            <label for="">Tên hiện thị</label>
                            <input type="text" class="form-control name_edit" name="name"
                                   required>
                            <label for="">Nội dung</label>
                            <input type="text" class="form-control content_edit" name="content"
                                   required>
                            <span class="text-danger error-name"></span>
                        </div>
                        <div class="notice_category_edit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" id="update_category"><i class="fa fa-plus">
                                Sửa danh mục
                            </i>
                        </button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal" >Trở về
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
