@extends('admin.layout')
@section('content')



    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
            <div class="x_title">
                <h2>Danh sách người dùng</h2>

                <a type="button" class="btn btn-primary" href="{{ route('users.create') }}" style="margin-left: 20%"><i class="fa fa-plus-circle"></i> Thêm mới</a>
            </div>

            <div class="x_content">

                <div class="table-responsive">
                    <div class="col-10">
                        <form method="get" action="{{route('users.index')}}" class=" p-1 d-flex"
                              id="subjectFormSearch" enctype="multipart/form-data">
                            <div class="col-md-4">
                                <lable class="text-primary" for="name">Tên người dùng</lable>
                                <input value="{{ request()->input('name') }}" class="h-50" type="text" placeholder="Tên tìm kiếm..." name="name"  >
                            </div>
                            <div class="col-md-3">
                                <lable class="text-primary" for="role" >Quyền </lable>
                                <select name="role" class="h-50 classroom-select-faculty">
                                    <option value="">Tất cả</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->name}}" {{(request()->input('role')==$role->name)?'selected':''}}>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="">
                                <button class="btn btn-primary  aqua-gradient btn-rounded btn-sm my-0" type="submit" title="Tìm kiếm" >
                                    <i class="fa fa-search"></i>Tìm kiếm
                                </button>
                            </div>

                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <table class="table table-striped jambo_table bulk_action" id="listBrands">
                        <thead>
                        <tr class="headings">
                            <th class="column-title">STT </th>
                            <th class="column-title">Tên </th>
                            <th class="column-title">Email </th>
                            <th class="column-title">Avatar </th>
                            <th class="column-title">Quyền </th>
                            <th class="column-title no-link last" style="width: 15%"><span class="nobr">Action</span>
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $stt = 0 ?>
                        @foreach($users as $user)
                            <?php $stt = $stt +1 ?>
                            <tr class="even pointer">
                                <td class=" ">{{ $stt }}</td>
                                <td class=" ">{{ $user->name }}</td>
                                <td class=" ">{{ $user->email }}</td>
                                <td class=" ">
                                    <img src="{{ asset('admin/upload/users/'. $user->avatar )}}" height="100" width="100">
                                </td>
                                <td class=" ">

                                    @foreach($user->roles as $r)
                                        <p>{{ $r->name }}</p>
                                    @endforeach
                                </td>
                                <td class=" ">
                                    <a  href="{{ route('users.edit', $user->id) }}" class="btn btn-info btn-xs" > <i class="fa fa-pencil"></i> Sửa </a>
                                    <button class="btn btn-danger btn-xs delete_user" editUser="{{$user->id}}"><i class="fa fa-trash-o"></i> Xóa </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{$users->appends(Request::all())->links()}}
        </div>
    </div>
@endsection
