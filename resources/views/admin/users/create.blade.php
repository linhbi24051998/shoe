@extends('admin.layout')
@section('content')
    <style>
        .error
        {
            color: red;
        }
    </style>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <h4>Thêm người dùng</h4>
                <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="{{ route('users.store') }}" method="post"  enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tên người dùng</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" value="{{old('name')}}" placeholder="Nhập tên sản phẩm" name="name">
                                @error('name')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="email" class="form-control" value="" name="email">
                                @error('email')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Password </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="password" class="form-control" value="" name="password">
                                @error('password')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Ảnh đại diện</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="file" name="avatar" id="autocomplete-custom-append" class="form-control col-md-10 image-input"/>
                                <div class="display_img">
                                    <img src="" alt="" width="300px" height="300px"
                                         style="max-height: 100%;max-width: 100%" class="image-show">
                                    @error('image')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nhóm quyền</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="js-example-basic-multiple" name="role[]" multiple="multiple" style="width: 50%;">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }} " {{ in_array($role->id, old('role', [])) ? 'selected' : '' }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <a type="button" class="btn btn-primary" href="{{ route('users.index') }}">Trở lại</a>
                                <button type="reset" class="btn btn-primary">Tải lại</button>
                                <button type="submit" class="btn btn-success">Thêm mới</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
