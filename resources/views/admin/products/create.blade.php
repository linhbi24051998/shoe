@extends('admin.layout')
@section('content')
    <style>
        .error
        {
            color: red;
        }
    </style>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <h4>Thêm sản phẩm</h4>
                <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="{{ route('product.store') }}" method="post"  enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tên sản phẩm</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" placeholder="Nhập tên sản phẩm" name="name">
                                @error('name')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Nội dung <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control" rows="3" placeholder="Nội dung" name="content"></textarea>
                                @error('content')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Giá sản phẩm</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" value="" name="price">
                                @error('price')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Số lượng sản phẩm</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="number" class="form-control" value="" name="number">
                                @error('number')
                                <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Ảnh đại diện</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="file" name="image" id="autocomplete-custom-append" class="form-control col-md-10 image-input"/>
                                <div class="display_img">
                                    <img src="" alt="" width="300px" height="300px"
                                         style="max-height: 100%;max-width: 100%" class="image-show">
                                    @error('image')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Ảnh chi tiết</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="file" id="uploadFile" name="uploadFile[]" enctype="multipart/form-data" multiple/>
                                <div class="display_img">
                                    <img src="" alt="" width="300px" height="300px"
                                         style="max-height: 100%;max-width: 100%" class="multi_image-show">
                                    @error('uploadFile')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Hãng sản phẩm</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control" name="category_id">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" >{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Loại sản phẩm</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="select2_group form-control" name="brand_id">
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}" > {{ $brand->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Sản phẩm mới</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="radio">
                                    <label>
                                        <input type="radio" checked="" value="1" id="optionsRadios1" name="status">Mới
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" value="0" id="optionsRadios2" name="status"> Không
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <a type="button" class="btn btn-primary" href="{{ route('product.index') }}">Trở lại</a>
                                <button type="reset" class="btn btn-primary">Tải lại</button>
                                <button type="submit" class="btn btn-success">Thêm mới</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
