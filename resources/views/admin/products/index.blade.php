@extends('admin.layout')
@section('content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Table design <small>Product design</small></h2>

            <div class="x_content">
                <div class="table-responsive">
                    <button class="btn btn-info delete_multiple" > <i class="fa fa-pencil"></i> Xóa nhiều </button>
                    <a class="btn btn-dark" href="{{ asset('admin/product/create') }}" >Thêm</a>
                    <div class="col-10">
                        <form method="get" action="{{route('product.index')}}" class=" p-1 d-flex"
                              id="subjectFormSearch" enctype="multipart/form-data">
                            <div class="col-md-4">
                                <lable class="text-primary" for="name">Tên sản phẩm</lable>
                                <input value="{{ request()->input('name') }}" class="h-50" type="text" placeholder="Tên tìm kiếm..." name="name"  >
                            </div>

                            <div class="col-md-3">
                                <lable class="text-primary" for="role" >Tên loại sản phẩm</lable>
                                <select name="category" class="h-50 classroom-select-subject">
                                    <option value="">Tất cả</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->name}}" {{(request()->input('category')==$category->name)?'selected':''}}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <lable class="text-primary" for="role" >Tên hãng </lable>
                                <select name="brand" class="h-50 classroom-select-faculty">
                                    <option value="">Tất cả</option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->name}}" {{(request()->input('brand')==$brand->name)?'selected':''}}>{{$brand->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="">
                                <button class="btn btn-primary  aqua-gradient btn-rounded btn-sm my-0" type="submit" title="Tìm kiếm" >
                                    <i class="fa fa-search"></i>Tìm kiếm
                                </button>
                            </div>

                        </form>
                        <div class="clearfix"></div>
                    </div>

                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th class="column-title" style="display: table-cell;"><input type="checkbox" id="check_multiple">KM</th>
                            <th class="column-title" style="display: table-cell;">Ảnh</th>
                            <th class="column-title" style="display: table-cell;">Tên </th>
                            <th class="column-title" style="display: table-cell;">Loại sản phẩm </th>
                            <th class="column-title" style="display: table-cell;">Hãng sản phẩm </th>
                            <th class="column-title" style="display: table-cell;">Trạng thái </th>
                            <th class="column-title" style="display: table-cell;">Nội dung </th>
                            <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Thao tác </span>
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($products as $product)
                            <tr class="even pointer" id="check_delete_{{ $product->id }}">
                                <td><input type="checkbox" class="check_1" data-id="{{ $product->id }}"></td>
                                <td class=" "><img src="{{ asset('admin/upload/products/'. $product->image )}}" alt="{{ $product->img }}" style="width: 100px;"></td>
                                <td class=" ">{{ $product->name }}</td>
                                <td class=" " data-id="{{ $product->category->id }}">{{ $product->category->name }}</td>
                                <td class=" " data-id="{{ $product->brand->id }}">{{ $product->brand->name }}</td>
                                <td>{{ $product->status == 1 ? 'Mới' : 'Không' }}</td>
                                <td class=" ">
                                    <p>{{ $product->content }}</p>
                                    <strong> Số lượng: {{ $product->number }}</strong><br>
                                    <strong style="color: red"> Giá: {{ number_format($product->price,3) }}</strong>
                                </td>
                                <td class=" ">
                                    <a class="btn btn-primary" href="{{ route('product.edit', $product->id) }}" >Sửa</a>
                                    <a class="btn btn-danger delete_product" data-id="{{ $product->id }}">Xóa</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{$products->appends(Request::all())->links()}}
        </div>
@endsection

