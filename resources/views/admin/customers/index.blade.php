@extends('admin.layout')
@section('content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Table design <small>Custom design</small></h2>

            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="table-responsive">
                <button id="delete_all">Xóa hết</button>
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                    <tr class="headings">
                        <th>
                            <input type="checkbox" id="check-all">
                        <th class="column-title" style="display: table-cell;">Tên </th>
                        <th class="column-title" style="display: table-cell;">Ảnh</th>
                        <th class="column-title" style="display: table-cell;">Địa chỉ </th>
                        <th class="column-title" style="display: table-cell;">Điện thoại </th>
                        <th class="column-title" style="display: table-cell;">Email </th>
                        <th class="column-title" style="display: table-cell;">Ghi chú </th>
                        <th class="column-title" style="display: table-cell;">Trạng thái </th>
                        <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Thao tác </span>
                        </th>
                        <th class="bulk-actions" colspan="7" style="display: none;">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($customers as $customer)
                        <tr class="even pointer" id="delete_{{ $customer->id }}">
                            <td class="a-center ">
                                <input type="checkbox" class="check_one" data-id="{{ $customer->id }}">
                            </td>
                            <td class=" ">{{ $customer->name }}</td>
                            <td class=" "><img src="{{ asset('admin/upload/customers/'. $customer->avatar )}}" alt="{{ $customer->avatar }}" style="width: 100px;"></td>
                            <td class=" ">{{ $customer->address }}</td>
                            <td class=" ">{{ $customer->phone }}</td>
                            <td class=" ">{{ $customer->email }}</td>
                            <td class="a-right a-right ">{{ $customer->note }}</td>
                            @csrf
                            <td class="a-right a-right change_status" replace="{{ $customer->id }}">
                                {{$customer->status == 1 ?'cho phép' :'chặn'}}
                            </td>
                            <td class=" last" >
                                <button class=" btn btn-primary block" id="sts_{{ $customer->id }}" data-id="{{$customer->id}}">
                                    {{$customer->status == 0 ?'cho phép' :'chặn'}}
                                </button>

                                <button class="btn btn-dark delete_customer" data-id="{{ $customer->id }}" >Xóa</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$customers->appends(Request::all())->links()}}
        </div>
    </div>
@endsection


