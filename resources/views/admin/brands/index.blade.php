@extends('admin.layout')
@section('content')
    <style>
        .add{
            margin-left: 20%;
        }
        .notice_brand{
            color: red;
        }
    </style>
    <div class="row">
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Danh sách hãng</h2>
                    <div class="title_right">
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <form method="get" action="{{ route('brands.index') }}">
                                    <input style="width: 50%" type="text" class="form-control" value="{{ request()->input('name') }}" placeholder="Hãng tìm kiếm..." name="name">
                                    <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Go!</button>
                                            </span>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Large modal -->
                    <button type="button" class="btn btn-primary add" data-toggle="modal" data-target=".add_brand"><i class="fa fa-plus-circle"></i> Thêm mới</button>
                    <!-- end modal -->
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action" id="listBrands">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">Tên </th>
                                <th class="column-title">Nội dung </th>
                                <th class="column-title no-link last" style="width: 15%"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($brands as $brand)
                                <tr class="even pointer">
                                    <td class=" ">{{ $brand->name }}</td>
                                    <td class=" ">{{ $brand->content }}</td>
                                    <td class=" ">
                                        <button class="btn btn-info btn-xs editBrand" data-toggle="modal" data-target=".edit_brand" editbraId="{{$brand->id}}" > <i class="fa fa-pencil"></i> Sửa </button>
                                        <button class="btn btn-danger btn-xs deleteBrand" editbraId="{{$brand->id}}"><i class="fa fa-trash-o"></i> Xóa </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal add -->
            @include('admin.brands.create')
        <!-- modal edit -->
            @include('admin.brands.edit')
        {{$brands->appends(Request::all())->links()}}
    </div>

@endsection
